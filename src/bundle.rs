use crate::system::InputManagementSystemDesc;
use amethyst_core::shred::{DispatcherBuilder, World};
use amethyst_core::{SystemBundle, SystemDesc};
use amethyst_error::Error;
use amethyst_input::BindingTypes;
use std::marker::PhantomData;

/// Bundle to add Bami input utility.
///
/// This bundle should be added AFTER the `InputBundle`.
/// The type `B` should be the same bindings as the bindings used for the `InputBundle`.
///
/// Also adds controller support with the `gilrs` feature enabled.
#[derive(Default)]
pub struct BamiBundle<B: BindingTypes> {
    phantom: PhantomData<B>,
}

impl<'a, 'b, B: BindingTypes> SystemBundle<'a, 'b> for BamiBundle<B> {
    fn build(
        self,
        world: &mut World,
        dispatcher: &mut DispatcherBuilder<'a, 'b>,
    ) -> Result<(), Error> {
        #[cfg(feature = "gilrs")]
        {
            use crate::GilRsControllerSystem;

            dispatcher.add(
                GilRsControllerSystem::<B>::default(),
                "gilrs_input_system",
                &[],
            );
        }
        dispatcher.add(
            InputManagementSystemDesc::<B>::default().build(world),
            "input_management_system",
            &[],
        );
        Ok(())
    }
}
