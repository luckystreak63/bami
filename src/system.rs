use crate::input::Input;
use amethyst_core::ecs::prelude::{ComponentEvent, SystemData};
use amethyst_core::ecs::{
    Entities, Join, Read, ReaderId, System, World, WorldExt, Write, WriteStorage,
};
use amethyst_core::SystemDesc;
use amethyst_input::{BindingTypes, InputHandler};
use std::marker::PhantomData;

#[derive(Debug)]
pub struct InputManagementSystemDesc<B: BindingTypes> {
    phantom: PhantomData<B>,
}

impl<'a, 'b, B: BindingTypes> SystemDesc<'a, 'b, InputManagementSystem<B>>
    for InputManagementSystemDesc<B>
{
    fn build(self, world: &mut World) -> InputManagementSystem<B> {
        <InputManagementSystem<B> as System<'_>>::SystemData::setup(world);

        let mut locals = WriteStorage::<Input<B>>::fetch(&world);
        let events_id = locals.register_reader();

        InputManagementSystem::new(events_id)
    }
}

impl<B: BindingTypes> Default for InputManagementSystemDesc<B> {
    fn default() -> Self {
        InputManagementSystemDesc {
            phantom: Default::default(),
        }
    }
}

/// Updates input statistics, ideally called at the start of every frame
pub struct InputManagementSystem<B: BindingTypes> {
    events_id: ReaderId<ComponentEvent>,
    phantom_bindings: PhantomData<B>,
}

impl<B: BindingTypes> InputManagementSystem<B> {
    pub(crate) fn new(events_id: ReaderId<ComponentEvent>) -> InputManagementSystem<B> {
        Self {
            events_id,
            phantom_bindings: Default::default(),
        }
    }
}

impl<'s, B: BindingTypes> System<'s> for InputManagementSystem<B> {
    type SystemData = (
        Read<'s, InputHandler<B>>,
        Write<'s, Input<B>>,
        WriteStorage<'s, Input<B>>,
        Entities<'s>,
    );

    fn run(&mut self, (input_handler, mut input, mut input_comp, entities): Self::SystemData) {
        let mut inserted = Vec::new();
        input_comp
            .channel()
            .read(&mut self.events_id)
            .for_each(|event| match event {
                ComponentEvent::Inserted(id) => {
                    inserted.push(*id);
                }
                _ => (),
            });

        for id in inserted {
            if let Some(manager) = input_comp.get_mut(entities.entity(id)) {
                manager.init(&input_handler);
            }
        }

        // Per entity emulated input
        for (manager, _) in (&mut input_comp, &entities).join() {
            manager.update_emulated();
        }

        // Global input
        for action in input_handler.bindings.actions() {
            let action_is_down = input_handler.action_is_down(action).unwrap_or(false);
            input.actions.update_statistics(action, action_is_down);
        }

        for axis in input_handler.bindings.axes() {
            let action_is_down = if let Some(value) = input_handler.axis_value(axis) {
                let stats = input.axes.statistics.get_mut(axis).unwrap();
                stats.set_axis_value(value)
            } else {
                false
            };

            input.axes.update_statistics(axis, action_is_down);
        }
    }

    fn setup(&mut self, world: &mut World) {
        let mut result = Input::<B>::default();

        {
            let input = world.read_resource::<InputHandler<B>>();
            result.init(&*input);
        }
        world.insert(result);
    }
}

#[cfg(test)]
mod tests {
    use crate::system::{InputManagementSystem, InputManagementSystemDesc};
    use crate::Input;
    use amethyst_core::ecs::{Builder, RunNow, World, WorldExt};
    use amethyst_core::SystemDesc;
    use amethyst_input::{Axis, Button, InputHandler, StringBindings, VirtualKeyCode};

    fn input_world() -> (World, InputManagementSystem<StringBindings>) {
        let mut world = World::new();
        world.insert(InputHandler::<StringBindings>::new());

        {
            let mut strings = world.write_resource::<InputHandler<StringBindings>>();
            strings
                .bindings
                .insert_action_binding(String::from("test"), vec![Button::Key(VirtualKeyCode::A)])
                .expect("");
            strings
                .bindings
                .insert_axis(String::from("axis"), Axis::MouseWheel { horizontal: false })
                .expect("");
        }

        let mut ims = InputManagementSystemDesc::<StringBindings>::default().build(&mut world);
        ims.setup(&mut world);

        (world, ims)
    }

    #[test]
    fn emulated_input() {
        let (mut world, mut input_sys) = input_world();

        let entity = world
            .create_entity()
            .with(Input::<StringBindings>::default())
            .build();

        let test_action = String::from("test");
        let test_axis = String::from("axis");

        {
            let mut storage = world.write_storage::<Input<StringBindings>>();
            let input = storage.get_mut(entity.clone()).unwrap();
            input.emulated_actions.push(test_action.clone());
            input.update_emulated();

            // Bindings have not been inserted into hashmap yet
            assert_eq!(input.actions.single_press(&test_action).is_down, false);
        }

        // Insert bindings into hashmap
        input_sys.run_now(&world);
        world.maintain();

        {
            let mut storage = world.write_storage::<Input<StringBindings>>();
            let input = storage.get_mut(entity.clone()).unwrap();
            input.emulated_actions.push(test_action.clone());
            input.update_emulated();

            // Now input works
            assert!(input.actions.single_press(&test_action).is_down);

            // Pressing nothing releases the key as expected
            input.update_emulated();
            assert!(input.actions.just_released(&test_action));

            // Test axis down
            input.emulated_axes.push((test_axis.clone(), 0.7));
            input.update_emulated();
            assert_eq!(input.axes.single_press(&test_axis).axis, 0.7);

            // test axis up, even with event with value == 0.0
            input.emulated_axes.push((test_axis.clone(), 0.0));
            input.update_emulated();
            assert!(input.axes.just_released(&test_axis));
        }
    }
}
