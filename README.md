# `bami`

The basic amethyst input library. Offers simple utility for input with the [amethyst](https://github.com/amethyst/amethyst) crate.

Controller support can be added through the [gilrs](https://gitlab.com/gilrs-project/gilrs) feature :
```toml
[dependencies.bami]
version = "0.2.0"
features = ["gilrs"]
```

This crate is a work in progress, some controller bindings may not yet be supported.

## Setup :
```rust
use bami::BamiBundle;

let game_data = GameDataBuilder::default()
    ...
    .with_bundle(BamiBundle::<StringBindings>::default())?
    // ^------ Add this bundle AFTER the `InputBundle`
    // Feel free to replace `StringBindings` with a custom type
    ...
```

## Usage :
Feel free to replace `StringBindings` with a custom type in your code!
```rust
use bami::{Input};

// Inside a system
type SystemData = Read<'s, Input<StringBindings>>;
    
fn run(&mut self, input: Self::SystemData) {
    let action = String::from("jump");

    // Only true the first frame of a same press
    let pressed_this_frame = 
        input.actions.single_press(&action).is_down;

    // `true` every frame the action is held
    // Represents the raw input state.
    let being_held_down: bool =
        input.actions.status(&action).is_down;

    let axis = String::from("horizontal");

    // Axis value between [-1.0, 1.0].
    let walk_speed: f32 =
        input.axes.status(&axis).axis;

    // Axes can also be used for menus with `single_press`.
    // Every frame where the axis is not at 0.0 is considered
    //   a press. On the frames where `single_press` would
    //   return `is_down=false`, the axis value will also be 0.0
    let menu_axis =
        input.axes.single_press(&axis).axis;

    if menu_axis > 0.0 {
        // Pressed right
    } else if menu_axis < 0.0 {
        // Pressed left
    }
}
```

## Run tests:
```
cargo test --features amethyst/empty
```
